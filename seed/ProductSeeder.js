var Product = require('../models/product');
var mongoose = require('mongoose');
mongoose.connect('localhost:27017/shopping');
/* DUPLICATE THIS PRODUCT TO TEST. ADDITIONAL IMAGES IN IMG/BUNDLES */
var products = [
  new Product({

   name: "PRODUCT ONE",
   productImagePath: "/img/bundles/p1.jpg",
   space: "SPACE ONE",
   climate: "CLIMATE ONE",
   small: [
                {
                  price: 140,
                  description: "lorem ipsum dolor sit amet",
                  button: "SMALL BUTTON INFO"
                }

              ],
   large: [
                {
                  price: 240,
                  description: "Nullam porttitor eleifend.",
                  button: "LARGE BUTTON INFO"
                }

              ]
 })


 /* STATIC PRODUCT EXAMPLE */

  new Product({
    name: 'PRODUCT STATIC EXAMPLE',
    subheading:'This is a subheading',
    bannerImagePath: 'img/product/static_example.jpg',
    description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse non pulvinar odio. Sed iaculis dapibus eros mollis egestas. Vestibulum ante ipsum primis',
    price: 505
  })
];

var done = 0;
for (var i = 0; i < products.length; i++) {
  products[i].save(function(err, result) {
    done++;
    if(done === products.length) {
      exit();
    }
  });
}

function exit() {
  mongoose.disconnect();
}
