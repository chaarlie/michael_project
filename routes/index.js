var express = require('express');
var router = express.Router();
var Product = require('../models/product');



/* GET bundle page. This is what youre working on. */
router.get('/bundle', function (req, res, next) {
	res.render('shop/bundle', {title: 'Title'});
});

router.post('/products', function (req, res, next) {
  console.log(req.body);
  var spaceValue = req.body.spaceValue;
  var climateValue = req.body.climateValue;
  
  // just a simple query
  // like SELECT * from PRODUCT WHERE space == spaceValue && climate == climateValue 
  // the {_id:0 }, ... selects all fields EXCEPT _id it's like a switch : 0|1
  Product.findOne(
  {
  	space: spaceValue, 
  	climate: climateValue
  }, 
  {_id:0 }, 
  function(err, docs) {
  	if(err)
  		console.log("an error occured", err);
	    
    if(docs){
    	if(!docs.length  > 0)
    		console.log("docs is empty");
    	
    	res.json(docs);
	  }
	  else
		  console.log('there are no results matching the criteria');
	});
  /* 
      basically 
      if we have an error is because err has a value and docs doesn't, in which case only line 28 runs. 
      If we  don't,  docs is not null, we could do if(docs && docs.length > 0) in one single line too
      else simply the values from the select dropdown don't exists...
    */
});

/* GET examplepage. This is just here so you can see an example of handlebars. Replace this ID with one that mongdoDB creates to test. */
router.get('/examplepage', function (req, res, next) {
  Product.find({"_id": ("5954324d985b1ee219f3c9d0")}).findOne(function(err, docs) {
 res.render('shop/examplepage',{title:'examplepage', products:docs});
  });
});


module.exports = router;
