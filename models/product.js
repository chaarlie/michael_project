var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var schema = new Schema({
  name: {type: String, required: true},
  subheading: {type: String, required: true},
  bannerImagePath: {type: String, required: true},
  productImagePath: {type: String, required: true},
  description: {type: String, required: true},
  button: {type: String, required: true},
  price: {type: Number, required: true}

});

module.exports = mongoose.model('Product', schema, "Product");